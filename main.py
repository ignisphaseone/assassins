#!flask/bin/python
"""
    main.py
    ===
    Vectra Fun turns into a real thing! It's time for the Assassins' Pool
    Helper!
"""
from __future__ import (
    division, absolute_import, print_function, unicode_literals)

import argparse
from pas import app

argparser = argparse.ArgumentParser()
argparser.add_argument(
    '-d', '--debug', help='debug mode', default=False, action='store_true')


def main():
    args = argparser.parse_args()
    app.run(debug=args.debug)

if __name__ == '__main__':
    main()
