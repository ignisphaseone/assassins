# The Rules of Assassins #

## Objectives ##

You have a target and a pursuer. The goal of the game is to attack *only* your target, and to protect yourself from your pursuer. Hit only the target's ball, and leave your pursuer with a tough shot!

## Setup ##

Assassins is designed for a lot of people playing pool at once. For each player, give them a ball. The person who takes the '2' ball shoots first. Put the '1' ball in the front of the triangle so it's hit on the break. Place the '2' ball in the middle of the triangle (spot 5).

## Break Rules ##

The balls must hit the rail on break. Otherwise, reset and break again.

## Scratch Rules ##

When you shoot, the cue ball must hit a target's ball before any other balls, or it is a scratch. If you sink the cue ball or it comes off the table, that is also a scratch. If the 40 second shot clock ticks down, that is a scratch.

If you scratch, your turn is over, and your pursuer may place the cue ball anywhere on the table. Pull any target balls back out.

## Kill Rules ##

If you are trying to eliminate a player by sinking their final ball, you must call the eliminating shot in order to kill them. If you eliminate a player, you pull their final ball out and take it as your own (1UP).

If you are trying to attack a target with multiple balls, you do not need to call your shots. If you sink one of their balls, they stay in.

If you accidently sink a ball of your own, it stays in. If you eliminate yourself this way, you should feel shame (and your turn is over)!

If you cause sink any balls that are non-target balls, pull them back out. You can only kill your target (and yourself)!

## Rules As Written On The Whiteboard! ##

1. Balls must hit rail on break only.
2. Scratches can be placed anywhere.
3. Your balls stay in if you make them.
4. Target balls come out on scratch.
5. Scratch when you hit non-target balls first, or cue ball goes in, or comes off the table.
6. Non-target balls come out.
7. You must call eliminating shots, otherwise the ball comes out.
8. 40 second shot clock or you lose your turn.
9. When you finish off a player, you get their last ball as your own.