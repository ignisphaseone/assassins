"""
    module pas
    ===
    Vectra Fun is real! The first thing is an Assassins' Pool Web Tracker.
    Replace the whiteboard! Show everyone the rules!
"""
from __future__ import (
    division, absolute_import, print_function, unicode_literals)
from flask import Flask

app = Flask(__name__)

from pas import views
