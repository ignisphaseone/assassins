"""
    views.py
    ===
    Doc things go here.
"""
from __future__ import (
    division, absolute_import, print_function, unicode_literals)

from flask import render_template

from pas import app
from pas.forms import PlayerForm


@app.route('/')
def index():
    return render_template('base.html')
